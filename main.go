package main

import (
	"fmt"
	"os/exec"
	"time"
)

func ToWebM(inputPath string) error {
	_, err := exec.Command(
		"ffmpeg",
		"-i",
		inputPath,
		"-c:v",
		"libvpx-vp9",
		"-c:a",
		"libopus",
		"-b:v",
		"0",
		"-b:a",
		"32k",
		"-crf",
		"33",
		"-speed",
		"4",
		"-frame-parallel",
		"1",
		"/path/to/video.webm",
		"-y",
	).Output()

	return err
}

func ToOGG(inputPath string) error {
	_, err := exec.Command(
		"ffmpeg",
		"-i",
		inputPath,
		"-codec:v",
		"libtheora",
		"-qscale:v",
		"3",
		"-codec:a",
		"libvorbis",
		"-qscale:a",
		"3",
		"-f",
		"ogv",
		"/path/to/video.ogv",
		"-y",
	).Output()

	return err
}

func GetVideoCover(inputPath string) error {
	_, err := exec.Command(
		"ffmpeg",
		"-i",
		inputPath,
		"-vframes",
		"1",
		"/path/to/video-cover.jpeg",
		"-y",
	).Output()

	return err
}

func main() {
	start := time.Now().Unix()
	err := ToWebM("/path/to/video.mp4")

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	finished := time.Now().Unix()
	delta := finished - start
	message := fmt.Sprintf(`time to finished: %ds`, delta)

	fmt.Println(message)
}
